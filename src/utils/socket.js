import { setLocalStorage, getLocalStorage } from '.'

const Socket = {
    instance: null,
    pending: [],
    stateNames: {
        0: 'CONNECTING',
        1: 'OPEN',
        2: 'CLOSING',
        3: 'CLOSED'
    },
    init (baseURL, options = {}, accessToken = null) {
        baseURL = baseURL.replace('https://', 'ws://')
        baseURL = baseURL.replace('http://', 'ws://') + 'ws'
        if (accessToken != null && accessToken.length > 0) {
            baseURL += `?access_token=${accessToken}`
        }

        if (!this.pending || this.pending.length === 0) {
            const pending = getLocalStorage('chatMessageQueue')
            if (pending && typeof pending === 'object') {
                this.pending = pending
            }
        }

        this.instance = new WebSocket(baseURL)

        const defautlOnMsg = msg => console.log('Message received', msg)
        const defautlOnConn = msg => console.log('Connected')
        const defautlOnOpen = msg => console.log('Open')
        const defautlOnClose = msg => console.log('Closed')
        const defaultOnError = err => console.log('ERROR:', err)

        this.instance.onmessage = options.onmessage || defautlOnMsg
        this.instance.onconnect = options.onconnect || defautlOnConn
        this.instance.onopen = options.onopen || defautlOnOpen
        this.instance.onclose = options.onclose || defautlOnClose
        this.instance.onerror = options.onerror || defaultOnError

        return this.instance.readyState
    },
    addQueue (msg) {
        this.pending.push(msg)
        setLocalStorage('chatMessageQueue', msg)
    },
    close () {
        if (!this.instance) {
            return null
        }
        return this.instance.close()
    },
    send (data) {
        if (!this.instance) {
            return null
        }
        return this.instance.send(data)
    },
    state () {
        if (!this.instance) {
            return null
        }
        return this.stateNames[this.instance.readyState]
    },
    readyState () {
        if (!this.instance) {
            return null
        }
        return this.instance.readyState
    }
}

export default Socket

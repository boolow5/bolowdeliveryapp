import { API as api } from './api'
import { isValidEmail as isve } from './validations'
import fmt from './fmt'
import Vue from 'vue'
import SecureLS from 'secure-ls'
import { getDistanceFromLatLonInKm } from './geolocation'
import socket from './socket'

const ls = new SecureLS()

export const EventBus = new Vue()
export const isValidEmail = isve
export const timeSince = fmt.timeSince
export const Socket = socket

const localKeys = {
    queue: 'W5tmwWypC8',
    location_permission_status: 'sWj6dKUrEs',
    lastCoords: 'jxdCe3njpa',
    is_vendor: 'rNaqwAYb4Z',
    authUser: 'GSp6fsAjKH',
    userTypeTab: 'uPYmXaDF6j',
    categories: 'DUnnP2Wtrg',
    chatMessageQueue: 'Ynu8Xv2x3G',
    'magic-text': '5MLczMek7g',
    'location-access-allowed': 'qHd9MyVPLp'
}

export function setLocalStorage (key, value) {
    if (key in localKeys) {
        return ls.set(localKeys[key], value)
    }
    // return localStorage.setItem(key, value)
    return ls.set(key, value)
}

export function getLocalStorage (key) {
    if (key in localKeys) {
        return ls.get(localKeys[key])
    }
    // return localStorage.getItem(key)
    return ls.get(key)
}

export function removeLocalStorage (key) {
    if (key in localKeys) {
        return ls.remove(localKeys[key])
    }
    // return localStorage.removeItem(key)
    return ls.remove(key)
}

export function clearLocalStorage () {
    return ls.clear()
}

export function calculateDistance (coords1, coords2, unit) {
    if (coords1 && coords2 && 'lat' in coords1 && 'lng' in coords1 && 'lat' in coords2 && 'lng' in coords2) {
        const distance = getDistanceFromLatLonInKm(coords1.lat, coords1.lng, coords2.lat, coords2.lng)
        if (!unit || unit === 'km' || unit === 'kilometers') {
            return distance
        }
        if (unit === 'm' || unit === 'meter') {
            return distance * 1000
        }
        return distance
    }
}

export const API = api

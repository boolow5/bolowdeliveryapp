import {
  // optional!, for example below
  // with custom spinner
  //
  // QSpinnerIos,
  // QSpinnerGears,
  // QSpinnerCube,
  Loading
} from 'quasar'

import { setLocalStorage } from '.'

const debug = false

export const RequestQueue = {
  list: [],
  http: null,
  store: null,
  processing: null,
  timeout: 0,
  delay: 3000,
  isOnline: false,
  reconnectTimeout: 0,
  init (http, list, store) {
    console.debug('*********** [RequestQueue] ***********')
    this.http = http
    this.list = list || this.decryptList()
    this.store = store
  },
  add (item) {
    if (debug) {
      console.log('[RequestQueue] add', { item })
    }
    if (!item) {
      return
    }
    if (item && !item.id) {
      item.id = this.list.length
    }
    this.list.push(item)
    setLocalStorage('queue', this.encryptedList())
    return item
  },
  remove (item) {
    if (debug) {
      console.log('[RequestQueue] remove', { item })
    }
    const count = this.list.length
    this.list = this.list.filter(a => item && item.id ? item.id !== a.id : item !== a)
    // current count is less than the last count
    return this.list.length < count
  },
  encryptedList () {
    if (debug) {
      console.log('[RequestQueue] encryptedList')
    }
    return /* window.btoa */(JSON.stringify(this.list))
  },
  decryptList (rawString) {
    if (debug) {
      console.log('[RequestQueue] decryptList', { rawString })
    }
    return /* window.atob */ (rawString) || []
  },
  sendNext (isLoop = false) {
    if (this.list.length > 0) {
      if (debug) {
        console.log('[RequestQueue] sendNext', { isLoop })
      }
      const firstItem = this.list[0]
      this.sendRequest(firstItem)
    }
    if (isLoop) {
      this.loopRequests()
    }
  },
  sendRequest (request) {
    if (debug) {
      console.log('[RequestQueue] sendRequest', { request })
    }
    return new Promise((resolve, reject) => {
      if (!this.http) {
        reject('invalid http client: ' + typeof this.http)
        return
      }
      if (typeof request !== 'object' || request === null) {
        reject('invalid request type: ' + typeof request)
        return
      }
      if (this.processing === request.id) {
        reject('already processing this request: ' + request.id)
        return
      }
      this.processing = request.id
      const api = this.http.create({
        baseURL: request.baseURL,
        headers: request.headers,
        xhrFields: request.xhrFields
      })
      api({
        url: request.url || '/',
        method: request.method || request.METHOD || 'get',
        params: request.params || request.PARAMS || {},
        data: request.data || null
      }).then(resp => {
        this.processing = null
        this.setIsOnline(true)
        this.remove(request)
        resolve(resp)
      }).catch(err => {
        this.processing = null
        if (this.isNetworkError(err)) {
          this.setIsOnline(false)
        } else {
          this.setIsOnline(true)
        }
        reject('sendRequest failed: ' + err)
      })
    })
  },
  isNetworkError (err) {
    if (debug) {
      console.log('[RequestQueue] isNetworkError', { err })
    }
    return err.toString().indexOf('Network') !== -1 ||
      err.toString().indexOf('network') !== -1 ||
      err.toString().indexOf('NETWORK') !== -1
  },
  isUniqueConstraintError (err) {
    return err && err.indexOf('UNIQUE constraint failed:') > -1
  },
  setIsOnline (online) {
    this.isOnline = online === true
    if (this.store) {
      this.store.commit('setIsOnline', this.isOnline)
      if (!this.isOnline) {
        this.reconnectLoop()
      }
    }
  },
  reconnectLoop () {
    clearInterval(this.reconnectTimeout)
    this.reconnectTimeout = setTimeout(() => {
      this.loopRequests()
    }, this.delay)
  },
  loopRequests () {
    if (debug) {
      console.log('[RequestQueue] loopRequests', {
        requests: this.list.length,
        delay: this.delay,
        isOnline: this.isOnline
      })
    }
    clearInterval(this.timeout)
    this.timeout = setTimeout(() => {
      this.sendNext(true)
    }, this.delay || 500)
  },
  getInfo () {
    return {
      requests: this.list.length,
      delay: this.delay,
      isOnline: this.isOnline
    }
  }
}

export const DownloadFile = function (vm, options = {}, isExternal = false) {
  return new Promise((resolve, reject) => {
    const base = {}

    if (typeof options !== 'object') {
      options = {}
    }
    if (!options.headers) {
      options.headers = {
        'Content-Type': 'application/json'
      }
    }
    if (options.Accept) {
      options.headers.Accept = options.Accept
    }
    if (isExternal) {
      base.baseURL = options.baseURL || ''
    } else {
      base.baseURL = (typeof options.baseURL === 'string' ? options.baseURL : vm.$store.getters.getBaseURL) || ''
      if (!options.noAuthHeader) {
        options.headers.Authorization = `Bearer ${vm.$store.getters.jwt}`
        base.xhrFields = {
          withCredentials: true
        }
      }
    }

    base.headers = options.headers
    options.baseURL = typeof options.baseURL === 'string' ? options.baseURL : base.baseURL
    options.xhrFields = base.xhrFields

    options.method = 'get'

    const api = vm.$axios.create(base)

    api({
      url: options.url,
      method: options.method,
      params: options.params
    }).then(resp => {
      let parts = options.url.split('/')
      const url = window.URL.createObjectURL(new Blob([resp.data]))
      const link = document.createElement('a')
      link.href = url
      let fileName = 'download'
      if (parts.length > 0) {
        fileName = parts[parts.length - 1]
      } else {
        parts = options.url.split('.')
        if (parts.length > 0) {
          const ext = parts[parts.length - 1]
          fileName = `download.${ext}`
        }
      }
      link.setAttribute('download', fileName)
      document.body.appendChild(link)
      link.click()
    })
  })
}

export const API = function (vm, options = {}, isExternal = false) {
  if (debug) {
    console.log('API', { options, isExternal })
  }
  if (!options) {
    options = {}
  }
  if (options && options.loading === 'show') {
    Loading.show({
      // spinner: QSpinnerCube // QSpinnerGears // QSpinnerIos
    })
  }
  let request = {}
  return new Promise((resolve, reject) => {
    const base = {}
    if (typeof options.caching === 'undefined') {
      options.caching = false
    }
    if (typeof options.alertError === 'undefined') {
      options.alertError = true
    }
    if (!options.headers) {
      options.headers = {
        'Content-Type': 'application/json'
      }
    }
    if (isExternal) {
      base.baseURL = options.baseURL || ''
    } else {
      base.baseURL = vm.$store.getters.getBaseURL || ''
      if (!options.noAuthHeader) {
        options.headers.Authorization = `Bearer ${vm.$store.getters.jwt}`
        base.xhrFields = {
          withCredentials: true
        }
      }
    }

    base.headers = options.headers
    options.baseURL = base.baseURL
    options.xhrFields = base.xhrFields

    if (options && options.method === 'post' && options.caching) {
      request = RequestQueue.add({ ...options, isExternal })
    } else {
      request = { ...options, isExternal }
    }

    if (options && options.formData && Object.keys(options.formData).length > 0) {
      vm.$axios.post(
        options.url || '/',
        options.formData,
        options.formData.getHeaders()
      ).then(resp => {
        RequestQueue.setIsOnline(true)
        RequestQueue.remove(request)
        Loading.hide()
        resolve(resp)
      }).catch(err => {
        RequestQueue.setIsOnline(!RequestQueue.isNetworkError(err))
        if (err && RequestQueue.isOnline) {
          RequestQueue.remove(request)
        }
        console.debug(err)
        if (options.alertError) {
          const errors = parseErrorMessages(err)
          for (var i = 0; i < errors.length; i++) {
            if (RequestQueue.isUniqueConstraintError(errors[i])) {
              let parts = errors[i].split('.')
              if (parts.length > 0) {
                parts = parts[parts.length - 1]
                if (parts) {
                  errors[i] = `This ${parts} is already used`
                }
              }
            }
            if (errors[i].indexOf('Signature has expired.') === -1) {
              vm.$q.notify({
                type: 'negative',
                color: 'negative',
                position: 'top',
                message: errors[i]
              })
            }
          }
        }
        Loading.hide()
        reject(err)
      })
    }

    const api = vm.$axios.create(base)

    api({
      url: options.url || '/',
      method: options.method || options.METHOD || 'get',
      params: options.params || options.PARAMS || {},
      data: options.data || null
    }).then(resp => {
      RequestQueue.setIsOnline(true)
      RequestQueue.remove(request)
      Loading.hide()
      resolve(resp)
    }).catch(err => {
      RequestQueue.setIsOnline(!RequestQueue.isNetworkError(err))
      if (err && RequestQueue.isOnline) {
        RequestQueue.remove(request)
      }
      console.debug(err)
      if (options.alertError) {
        const errors = parseErrorMessages(err)
        for (var i = 0; i < errors.length; i++) {
          if (RequestQueue.isUniqueConstraintError(errors[i])) {
            let parts = errors[i].split('.')
            if (parts.length > 0) {
              parts = parts[parts.length - 1]
              if (parts) {
                errors[i] = `This ${parts} is already used`
              }
            }
          }
          if (errors[i].indexOf('Signature has expired.') === -1) {
            vm.$q.notify({
              type: 'negative',
              color: 'negative',
              position: 'top',
              message: errors[i]
            })
          }
        }
      }
      Loading.hide()
      reject(err)
    })
  })
}

export const parseErrorMessages = (err) => {
  const data = JSON.parse(JSON.stringify(err))
  console.log('handleAPIError', data)
  if (!data) {
    return
  }
  let errors = []
  if (typeof data === 'string') {
    errors = [data]
  } else if (data.response && data.response.data) {
    console.log('data.response.data.errors', {
      errors: data.response.data.errors,
      type: typeof data.response.data.errors,
      length: data.response.data.errors && data.response.data.errors.length,
      detail: data.response.data.detail,
      non_field_errors: data.response.data.non_field_errors
    })
    errors = data.response.data.errors
      ? data.response.data.errors
      : (data.response.data.error && [data.response.data.error]) || []

    if (JSON.stringify(errors) === '[]' && (data.response.data.detail || data.response.data.non_field_errors)) {
      errors = data.response.data.detail ? [data.response.data.detail] : data.response.data.non_field_errors
    }
    if (typeof errors === 'object' && errors.constructor && errors.constructor.name !== 'Array') {
      const temp = JSON.parse(JSON.stringify(errors))
      errors = []
      Object.keys(temp).forEach(key => {
        if (temp[key] && !Number.isNaN(parseInt(key))) {
          errors.push(temp[key])
        } else {
          errors.push(`${key.toLocaleUpperCase()}: ${temp[key]}`)
        }
      })
    }
    if (errors && errors.length === 0 && data.response.data.msg) {
      errors = [data.response.data.reason || data.response.data.msg]
    }
  }
  errors = errors.map(msg => {
    if (typeof msg === 'string' && msg.indexOf('[\'') > -1 && msg.length > 4) {
      console.log('parsing:', msg)
      return msg.substring(2, msg.length - 2)
    }
    return msg
  })
  console.log('ERRORS', errors)
  return errors
}

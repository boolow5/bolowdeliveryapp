import moment from 'moment'

const timeSince = (d, showSuffix = false) => {
  d = moment(d).fromNow(showSuffix)

      .replace('a second', '1s')
      .replace('a few seconds', '1s')
      .replace(' seconds', 's')
      .replace(' second', 's')

      .replace('a few minutes', '1m')
      .replace('a minute', '1m')
      .replace(' minutes', 'm')
      .replace(' minute', 'm')

      .replace('an few hours', '1h')
      .replace('an hour', '1h')
      .replace(' hours', 'h')
      .replace(' hour', 'h')

      .replace('a few days', '1d')
      .replace('a day', '1d')
      .replace(' days', 'd')
      .replace(' day', 'd')

      .replace('a few months', '1 month')
      .replace('a month', '1 month')
      .replace(' months', ' months')
      .replace(' month', ' month')

      .replace('a few years', '1 year')
      .replace('a year', '1 year')
      .replace(' years', ' years')
      .replace(' year', ' year')
  return d
}

export default {
  timeSince
}

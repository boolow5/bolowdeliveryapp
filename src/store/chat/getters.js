export function conversations (state) {
    return state.conversations
}

export function selectedConversation (state) {
    return state.selectedConversation
}

export function isInConversations (state) {
    return (conversationID) => state.conversations.filter(a => a && a.id === conversationID).length > 0
}

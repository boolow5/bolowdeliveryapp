export function setConversations (state, conversations) {
    state.conversations = conversations
}

export function selectedConversation (state, conversation) {
    state.selectedConversation = conversation
}

export function updateConversationMessages (state, msg) {
    if (msg && msg.conversation_id && msg.conversation_id.length === 24) {
        state.conversations = state.conversations.map(conversation => {
            if (conversation && conversation.id === msg.conversation_id) {
                conversation.messages.push(msg)
            }
            return conversation
        })
        if (state.selectedConversation && state.selectedConversation.id === msg.conversation_id) {
            state.selectedConversation.messages.push(msg)
        }
    }
}

import { getLocalStorage, setLocalStorage, removeLocalStorage } from 'src/utils'

export const isAuthenticated = () => {
  const token = getLocalStorage('magic-text')
  return Boolean(token && (typeof token === 'string' && token.split('.').length === 3))
}

const DEBUG = false

var DOMAIN = 'http://localhost:8072'
if (!DEBUG) {
  DOMAIN = 'https://shop.iweydi.com'
}

const state = {
  authUser: (getLocalStorage('authUser') && JSON.parse(getLocalStorage('authUser'))) || {},
  jwt: getLocalStorage('magic-text'),
  endpoints: {
    // TODO: Remove hardcoding of dev endpoints
    obtainJWT: `${DOMAIN}/api/v1/login`,
    refreshJWT: `${DOMAIN}/api/v1/refresh_token/`,
    baseURL: `${DOMAIN}/api/v1/`
  },
  usersCache: [],
  isVendor: getLocalStorage('is_vendor') === 'true' || getLocalStorage('is_vendor') === true
}

const getters = {
  authUser: (state) => state.authUser,
  userID: (state) => state.authUser && state.authUser.profile && state.authUser.profile.id,
  jwt: (state) => state.jwt,
  getBaseURL: (state) => state.endpoints.baseURL,
  getLoginEndpint: (state) => state.endpoints.obtainJWT,
  getRefreshToken: (state) => state.endpoints.refreshJWT,
  isAuthenticated (state) {
    return Boolean(state.jwt && (typeof state.jwt === 'string' && state.jwt.split('.').length === 3))
  },
  usersCache (state) {
    if (typeof state.usersCache === 'object' && state.usersCache.length === 0) {
      const usersCache = getLocalStorage('usersCache')
      if (usersCache) {
        return JSON.parse(usersCache)
      }
    }
    return state.usersCache
  },
  apiDomain: () => DOMAIN,
  baseURL: (state) => state.endpoints.baseURL,
  isVendor: (state) => state.isVendor
}

const mutations = {
  setAuthUser (state, { authUser }) {
    state.authUser = authUser
    setLocalStorage('authUser', authUser && authUser.id ? JSON.stringify(authUser) : '{}')
  },
  updateToken (state, newToken) {
    setLocalStorage('magic-text', newToken)
    state.jwt = newToken
  },
  updateIsVendor (state, isVendor) {
    setLocalStorage('is_vendor', isVendor)
    state.isVendor = isVendor
  },
  removeToken (state) {
    removeLocalStorage('magic-text')
    state.jwt = null
  },
  replaceUserCache (state, { newItem, index }) {
    if (index < state.usersCache.length) {
      state.usersCache[index] = newItem
    }
  },
  addToUserCache (state, { newItem }) {
    state.usersCache.push(newItem)
  }
}

const actions = {
  updateUsersCache ({ commit, state }, payload) {
    console.log('updateUsersCache', payload)
    if (typeof payload === 'object' && payload && payload.length > 0) {
      var foundUser = null
      payload.forEach(newItem => {
        foundUser = null
        for (var i = 0; i < state.usersCache.length; i++) {
          if (state.usersCache[i] && newItem && state.usersCache[i].id === newItem.id) {
            commit('replaceUserCache', { newItem, index: i })
            foundUser = newItem
          }
        }
        if (foundUser === null) {
          commit('addToUserCache', { newItem })
        }
      })
    }
    setLocalStorage('usersCache', JSON.stringify(state.usersCache))
  },
  clearUserData ({ commit }, payload) {
    commit('removeToken')
    commit('setAuthUser', { authUser: null })
    commit('updateToken', '')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

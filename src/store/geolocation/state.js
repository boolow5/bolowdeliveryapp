import { getLocalStorage } from 'src/utils'

export default function () {
  return {
    coordinates: typeof getLocalStorage('lastCoords') === 'string' && getLocalStorage('lastCoords').indexOf('{') === 0 ? JSON.parse(getLocalStorage('lastCoords')) : {
      lat: 0,
      lng: 0
    }
  }
}

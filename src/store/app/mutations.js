import { setLocalStorage } from '../../utils'

export function leftDrawerOpen (state, payload) {
    console.log('leftDrawerOpen', payload)
    state.leftDrawerOpen = (payload === true)
}

export function footerTab (state, payload) {
    console.log('footerTab', payload)
    state.footerTab = payload
}

export function userTypeTab (state, payload) {
    console.log('userTypeTab', payload)
    state.userTypeTab = payload
    setLocalStorage('userTypeTab', payload)
}

export function setCategories (state, payload) {
    state.categories = payload
    setLocalStorage('categories', JSON.stringify(payload))
}

export function toggleNotification (state, show) {
    state.showNotification = !show
}

export function setNotification (state, payload) {
    if (payload && typeof payload === 'object') {
        if (payload.constructor && payload.constructor.name === 'Array') {
            state.notifications = payload
        } else if (payload.constructor && payload.constructor.name === 'Object') {
            state.notifications.push(payload)
        }
    }
}

export function shareItem (state, item) {
    state.shareItem = item
}

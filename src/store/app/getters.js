export function leftDrawerOpen (state) {
    return state.leftDrawerOpen
}

export function appName (state) {
    return state.appName
}

export function appVersion (state) {
    return state.appVersion
}

export function footerTab (state) { return state.footerTab }

export function userTypeTab (state) { return state.userTypeTab }

export function categories (state) { return state.categories }

export function showNotification (state) { return state.showNotification }

export function notification (state) { return state.notification }

export function shareItem (state) { return state.shareItem }

import { getLocalStorage } from 'src/utils'

export default function () {
  return {
    leftDrawerOpen: false,
    appName: 'iWeydi',
    appVersion: '0.0.4',
    footerTab: 'home',
    userTypeTab: getLocalStorage('userTypeTab') || 'customer',
    categories: (getLocalStorage('categories') ? JSON.parse(getLocalStorage('categories')) : null) || [],
    showNotification: false,
    notification: [],
    shareItem: null
  }
}

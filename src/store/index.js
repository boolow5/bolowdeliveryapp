import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import app from './app'
import shop from './shop'
import geolocation from './geolocation'
import auth from './auth'
import chat from './chat'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app,
      shop,
      geolocation,
      auth,
      chat
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}

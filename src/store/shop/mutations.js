import { calculateDistance, getLocalStorage } from 'src/utils'

export function toggleCartItem (state, item) {
    const indices = state.cart.items.map(a => a && a.id)
    const index = indices.indexOf(item.id)
    console.log('toggleCartItem', { index, indices: indices.length })
    if (index > -1) { // found
        // remove item from cart
        state.cart.items.splice(index, 1)
    } else { // not found
        // add item to cart
        state.cart.items.push(item)
    }
}

export function setItems (state, items) {
    const currentCoords = JSON.parse(getLocalStorage('lastCoords'))
    if (items && items.length > 0) {
        items = items.map(item => {
            if (item.shop && item.shop.coords && 'lat' in item.shop.coords && 'lng' in item.shop.coords) {
                item.shop.distance = (calculateDistance(item.shop.coords, currentCoords, null)).toFixed(2)
            }
            return item
        })
    }
    state.items = items
}

export function setProductsError (state, error) {
    state.productsError = error || null
}

export function setCartItems (state, items) {
    state.cart.items = items
}

export function setCartItemsError (state, err) {
    state.cart.error = err || ''
}

export function updateCartItemQuantity (state, { id, quantity }) {
    console.log('updateCartItemQuantity', { id, quantity })
    state.cart.items = state.cart.items.map(item => {
        if (item && item.id === id) {
            item.order_quantity = parseInt(quantity) || 1
        }
        return item
    })
}

export function setVendors (state, shops) {
    const currentCoords = JSON.parse(getLocalStorage('lastCoords'))
    if (shops && shops.length > 0) {
        shops.map(shop => {
            shop.distance = (calculateDistance(shop.coords, currentCoords, null)).toFixed(2)
            return shop
        })
    }
    state.shops = shops.sort((a, b) => a && b && a.distance > b.distance ? -1 : 1)
}

export function shopsError (state, error) {
    state.shopsError = error || ''
}

export function setMyShops (state, myShops) {
    const currentCoords = JSON.parse(getLocalStorage('lastCoords'))
    if (myShops && myShops.length > 0) {
        myShops.map(shop => {
            shop.distance = (calculateDistance(shop.coords, currentCoords, null)).toFixed(2)
            return shop
        })
    }
    state.myShops = myShops.sort((a, b) => a && b && a.distance > b.distance ? -1 : 1)
}

export function monthDays (state, days) {
    state.monthDays = days
}

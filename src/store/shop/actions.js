export function addToCart ({ state, commit }, item) {
    if (!(item && item.id)) {
        console.warn('[addToCart] invalid item', item)
    }
    commit('toggleCartItem', item)
}

export default function () {
  return {
    cart: {
      items: [],
      error: null
    },
    shops: [],
    myShops: [],
    items: [],
    productsError: null,
    shopsError: null,
    monthDays: []
  }
}

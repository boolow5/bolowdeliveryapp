/*
export function someGetter (state) {
}
*/
export function cartItems (state) {
    return state.cart.items
}

export function cartItemsError (state) {
    return state.cart.error
}

export function cartItemsCount (state) {
    return state.cart.items.length
}

export function totalPrice (state) {
    if (state.cart.items && state.cart.items.length > 1) {
        console.log('[totalPrice] calculating...', state.cart.items)
        return state.cart.items.reduce((total = 0, item, index, items) => {
            console.log('[totalPrice] item-' + index, { price: item.price, total })
            if (typeof total === 'object') {
                total = parseFloat(total.price) * (parseInt(total.order_quantity || '1'))
            }
            return total + parseFloat(item.price) * (parseInt(item.order_quantity || '1'))
        })
    } else if (state.cart.items && state.cart.items.length === 1) {
        return parseFloat(state.cart.items[0].price) * parseInt(state.cart.items[0].order_quantity || '1')
    }
    return 0
}

export function cartItemsIDs (state) {
    return state.cart.items.map(a => a && a.id)
}

export function shops (state) {
    return state.shops
}

export function monthDays (state) {
    return state.monthDays
}

export function myShops (state) {
    return state.myShops
}

export function shopsError (state) {
    return state.shopsError
}

export function productsError (state) {
    return state.productsError
}


const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/vendors', component: () => import('pages/vendors.vue') },
      { path: '/vendors/:shopID', component: () => import('pages/VendorDetails.vue') },
      // customer pages
      { path: '/category/:categoryName', component: () => import('pages/Category.vue') },
      { path: '/shopping-cart', component: () => import('pages/Cart.vue') },
      { path: '/orders', component: () => import('pages/Orders.vue') },
      // Vendor pages
      { path: '/my-shops', component: () => import('pages/MyVendors.vue') },
      { path: '/my-products', component: () => import('pages/MyProducts.vue') },
      { path: '/customer-orders', component: () => import('pages/CustomerOrders.vue') },
      { path: '/chat', component: () => import('pages/Chat.vue') },
      { path: '/chat/:conversationID', component: () => import('pages/Chat.vue') },
      { path: '/settings', component: () => import('pages/Settings.vue') },
      { path: '/subscription', component: () => import('pages/Subscription.vue') },
      { path: '/terms-and-privacy', component: () => import('pages/TermsAndPrivacy.vue') }
    ]
  },
  {
    path: '/accounts',
    component: () => import('layouts/NoLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/accounts/Login.vue')
      },
      {
        path: 'login',
        component: () => import('pages/accounts/Login.vue'),
        meta: {
          isPublicPage: true
        }
      },
      {
        path: 'logout',
        component: () => import('pages/accounts/Login.vue'),
        meta: {
          isPublicPage: false
        }
      },
      {
        path: 'signup',
        component: () => import('pages/accounts/Signup.vue'),
        meta: {
          isPublicPage: true
        }
      },
      {
        path: 'reset-password',
        component: () => import('pages/accounts/ResetPassword.vue')
      },
      {
        path: 'forgot-password',
        component: () => import('pages/accounts/ForgotPassword.vue'),
        meta: {
          isPublicPage: true
        }
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes

#!/usr/bin/env python3

import json

file_name = 'missing-i18n-keys.json'
missing_keys = {}

print('Loading {}'.format(file_name))
with open(file_name, 'r') as f:
    output = json.load(f)

print('Parsing the output')
output = output.get('missingKeys', None)
if output is None:
    print('No missing translations found!')
    exit(1)

for key in output:
    missing_keys[key['path']] = key['path']
print('Done parsing')

print('Updating the file {}'.format(file_name))
with open(file_name, 'w') as f:
    json.dump(missing_keys, f)

print('Done!')
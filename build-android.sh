#!/bin/bash

# initialize colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

export BASE_DIR=$PWD
export CORDOVA_DIR=$BASE_DIR/src-cordova

bash $BASE_DIR/deployment-files/checkIsLocal.sh || exit 1;

echo "$green"
echo "$green Building Quasar project...$reset"
quasar build -m android

echo "$green"
echo "Preparing android...$reset"

cp build.json $CORDOVA_DIR;

KS_FILE=iweydi.keystore
KS_FILE_PATH="$CORDOVA_DIR/$KS_FILE"
if [ -f "$KS_FILE_PATH" ]
then
	echo "$green[FOUND]$reset: $KS_FILE in $CORDOVA_DIR"
else
	echo "[NOT FOUND] $KS_FILE in $CORDOVA_DIR"
    echo "$green Generating Keys...$reset"
    # keytool -genkey -v -keystore $KS_FILE_PATH -alias iweydi-mobile-debug -keyalg RSA -keysize 2048 -validity 10000
    keytool -genkey -v -keystore $KS_FILE_PATH -alias iweydi-mobile-release -keyalg RSA -keysize 2048 -validity 10000
    ln -s $KS_FILE_PATH $BASE_DIR
    echo "$green Done generating keys:$reset"
    keytool -exportcert -keystore $KS_FILE_PATH -list -v
fi

echo "$green Changing to $CORDOVA_DIR $reset";
cd $CORDOVA_DIR;
cordova build android --release --buildConfig $BASE_DIR/build.json && cd $CORDOVA_DIR/platforms/android/ && ./gradlew bundleRelease; open app/build/outputs/bundle/release/ && echo "$green DONE! $reset";

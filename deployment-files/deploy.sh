#!/bin/bash


# initialize colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# the dir that will house the whole thing
export HOST=139.162.169.72
# Package name
export API_PACKAGE=iweydi-shop-webapp.tar.gz
# The remote directory of the server
export DEPLOY_PATH=/webapps/iweydi-shop/app
# static files directory
export STATIC_PATH=/webapps/iweydi-shop/app/static
# remote server user
export OWNER=mahdi
# web user that runs the web app
export WEB_USER=bolowdeliveryapi
export WEB_GROUP=webapps
# process name
export PROC_NAME=bolowdeliveryapi

echo "${green}sending \"/tmp/$API_PACKAGE\" to \"$DEPLOY_PATH/bin\"${reset}"

scp -i "$HOME/.ssh/id_rsa" /tmp/$API_PACKAGE ${OWNER}@${HOST}:/tmp

ssh -i "$HOME/.ssh/id_rsa" -t ${OWNER}@${HOST} "
  sudo mkdir -p $DEPLOY_PATH || true;
  sudo tar -xvzf /tmp/$API_PACKAGE -C $DEPLOY_PATH;
  echo 'Restarting NINGX'
  sudo service nginx restart;
  echo 'DONE!'
";
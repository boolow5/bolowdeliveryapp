#!/bin/sh

# initialize colors
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

SETTINGS_FILE=$PWD/src/store/auth/index.js
echo "PWD: $PWD"

if grep -q "DEBUG = true" "$SETTINGS_FILE"; then
  echo "Change the app from debug mode to production"
  echo ""
  echo "Go to $SETTINGS_FILE and change the following line:"
  echo "${red}DEBUG = true"
  echo "${reset}to"
  echo "${green}DEBUG = false"
  echo "${reset}then run the script again to continue"
  echo ""
  exit 1;
fi
echo "${green}DEBUG = false${reset}"
